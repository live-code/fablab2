import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/auth.guard';

const routes: Routes = [
  { path: 'form1', loadChildren: () => import('./features/form1/form1.module').then(m => m.Form1Module) },
  { path: 'form2', loadChildren: () => import('./features/form2/form2.module').then(m => m.Form2Module) },
  { path: 'form3', canActivate: [AuthGuard], loadChildren: () => import('./features/form3/form3.module').then(m => m.Form3Module) },
  { path: 'form4', loadChildren: () => import('./features/forms4/forms4.module').then(m => m.Forms4Module) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'cms', loadChildren: () => import('./features/cms/cms.module').then(m => m.CmsModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
