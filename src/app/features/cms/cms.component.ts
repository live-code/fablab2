import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cgm-cms',
  templateUrl: './cms.component.html',
  styleUrls: ['./cms.component.css']
})
export class CmsComponent implements OnInit {

  constructor(private http: HttpClient) {
   http.get('http://localhost:3000/profile')
     .subscribe({
       next(val) { console.log(val)},
       error(err) { console.log('err', err)},
    })
  }

  ngOnInit(): void {
  }

}
