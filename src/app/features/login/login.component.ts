import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'cgm-login',
  template: `
    
    <input type="text">
    <input type="text">
    <button (click)="login()">SIGNIN</button>
  `,
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }


  login() {
    this.authService.login()
  }
}
