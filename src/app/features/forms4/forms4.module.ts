import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { Forms4RoutingModule } from './forms4-routing.module';
import { Forms4Component } from './forms4.component';


@NgModule({
  declarations: [
    Forms4Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    Forms4RoutingModule,
    ReactiveFormsModule
  ]
})
export class Forms4Module { }
