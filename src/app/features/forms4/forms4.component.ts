import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'cgm-forms4',
  template: `
    <h1>Form 4</h1>
    <form [formGroup]="form">
      <cgm-my-input
        formControlName="name"
        label="your name"
      ></cgm-my-input>
      
      <cgm-color-picker 
        formControlName="color"
      ></cgm-color-picker>
      
    </form>
    
    <pre>{{form.value | json}}</pre>
    <pre>{{form.valid}}</pre>
  `
})
export class Forms4Component implements OnInit {
  form = this.fb.group({
    name: ['', Validators.required],
    color: ['', Validators.required],
  })

  constructor(
    private fb: FormBuilder,
  ) {
    this.form.get('name')?.disable();

    setTimeout(() => {
      this.form.get('name')?.enable();
    }, 2000)
  }

  ngOnInit(): void {
  }

}
