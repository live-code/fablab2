import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Forms4Component } from './forms4.component';

const routes: Routes = [{ path: '', component: Forms4Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Forms4RoutingModule { }
