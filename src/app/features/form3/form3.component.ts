import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  catchError,
  combineLatest, concatMap,
  delay, exhaustMap, filter,
  first,
  forkJoin, fromEvent,
  interval,
  map,
  merge,
  mergeAll, mergeMap,
  of,
  switchMap,
  take, toArray
} from 'rxjs';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'cgm-form3',
  template: `
    <input type="text" [formControl]="input">
  `
})
export class Form3Component {
  input = new FormControl();
  @ViewChild('btn') btn!: ElementRef<HTMLButtonElement>

  constructor() {


  }

  ngAfterViewInit(): void {

  }

}

