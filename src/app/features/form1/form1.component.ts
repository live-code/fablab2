import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";

@Component({
  selector: 'cgm-form1',
  template: `

    <form [formGroup]="form">
      <div *ngIf="form.get('name')?.errors as err">
        <small *ngIf="err?.['required']">Field Required</small>
        <small *ngIf="err?.['minlength']">Field too short</small>
        <small *ngIf="err?.['alphaNumeric']">No Symbols!!!!</small>
      </div>

      <input type="checkbox" formControlName="isCompany">Are you a Company?

      <br>
      <input
        type="text" formControlName="name"
        placeholder="Name"
        [ngClass]="{ 'error': form.get('name')?.invalid && form.dirty}"
      >


      <!--VAT-->
      <br>
      <small *ngIf="form.get('vatCodFisc')?.errors?.['vat']">Invalid VAT</small>
      <br>
      <input type="text" formControlName="vatCodFisc"
             [placeholder]="form.get('isCompany')?.value ? 'VAT' : 'PERSON ID'">
      <button class="btn" [disabled]="form.invalid">SAVE</button>

      <h1 class="text-2xl">Anagrafica</h1>
      <div formGroupName="anagrafica">
        <div *ngIf="form.get('anagrafica')?.valid">form valido</div>
        <div *ngIf="form.get('anagrafica')?.valid">form valido</div>
        <div *ngIf="form.get('anagrafica')?.get('name')?.invalid">Name required</div>
        <div *ngIf="form.get('anagrafica.name')?.invalid">Name required</div>
        <input type="text" formControlName="name">
        <input type="text" formControlName="surname">
      </div>
    </form>

    <pre>{{form.get('name')?.errors | json}}</pre>

    <pre>value: {{form.value | json}}</pre>
    <pre>valid: {{form.valid}}</pre>
    <pre>dirty: {{form.dirty}}</pre>
    <pre>touched: {{form.touched}}</pre>
  `,
})
export class Form1Component {

  form = this.fb.nonNullable.group({
    isCompany: true,
    name: ['', [Validators.required, Validators.minLength(3), alphaNumericValidator]],
    vatCodFisc: [''],
    anagrafica: this.fb.group({
      name: ['A', Validators.required],
      surname: 'b'
    })

  })

  constructor(private fb: FormBuilder) {
    const obj = {
      name: 'A',
      isCompany: true,
      vatCodFisc: 'ABC',
      anagrafica: {
        name: 'Fab',
        surname: 'Ciccio'
      }
    }
    this.form.setValue(obj)

    this.form.get('isCompany')?.valueChanges
      .subscribe(isCompany => {
        if (isCompany) {
          this.form.get('vatCodFisc')?.setValidators(
            [Validators.required, (c: AbstractControl) => lengthValidator(c, 11)]
          )
          this.form.get('name')?.enable()
        } else {
          // user
          this.form.get('vatCodFisc')?.setValidators(
            [(c: AbstractControl) => lengthValidator(c, 16)]
          )
          this.form.get('name')?.disable()
        }
        this.form.get('vatCodFisc')?.updateValueAndValidity()
      })

    this.form.get('isCompany')?.setValue(true)

  }
}





const ALPHA_NUMERIC_REGEX = /^\w+$/

export function lengthValidator(c: AbstractControl, requiredLength: number): ValidationErrors | null {
  if (c.value && c.value.length !== requiredLength) {
    return {
      vat: true
    }
  }
  return null;
}

export function alphaNumericValidator(c: FormControl): ValidationErrors | null {
  if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
    return {
      alphaNumeric: true
    }
  }
  return null;
}
