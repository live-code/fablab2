import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormBuilder,
  FormControl,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import {
  catchError,
  debounceTime,
  first,
  firstValueFrom,
  map,
  Observable,
  of,
  startWith,
  switchMap,
  timer
} from 'rxjs';

@Component({
  selector: 'cgm-form2',
  template: `
    <form [formGroup]="form">
      <input type="text" formControlName="username">
      <pre>{{form.get('username')?.errors | json}}</pre>
      <div *ngIf="form.get('username')?.pending"> loading...</div>

      
      <div formGroupName="passwords">
        <div *ngIf="form.get('passwords')?.errors?.['passNotMatch']">passwords don't match</div>
        <input type="text" formControlName="password1" 
               [ngClass]="{'error': form.get('passwords.password1')?.invalid}">
        <input type="text" formControlName="password2" class="error"
               [ngClass]="{'error': form.get('passwords.password2')?.invalid}">
        
        <pre>{{form.get('passwords.password1')?.errors | json}}</pre>
        <pre>{{form.get('passwords.password2')?.errors | json}}</pre>
        
      </div>
      <button class="btn" [disabled]="form.invalid || form.pending">SAVE</button>
    </form>
    
    <pre>{{form.valid | json}}</pre>
    <pre>{{form.get('passwords')?.valid | json}}</pre>
  `,
})
export class Form2Component  {
  form = this.fb.nonNullable.group({
    username: ['', Validators.required, this.userValidator.checkUsername()],
    passwords: this.fb.group(
      {
        password1: ['', [Validators.required, Validators.minLength(3), alphaNumericValidator], ],
        password2: ['', [Validators.required, Validators.minLength(3), alphaNumericValidator], ],
      },
      {
        validators: passwordMatch()
      }
    )
  })

  constructor(
    private fb: FormBuilder, private userValidator: UserValidator, private http: HttpClient,

  ) {

    // ----------x|>
    this.http.get<any>(`https://jsonplaceholder.typicode.com/users`)
      .pipe(
        catchError(err => of(err))
      )
      .subscribe(
        res => {
          if (res instanceof HttpErrorResponse) {
            window.alert('ahia!')
          } else {
            console.log('result', res)
          }
        },
        err => console.log('errore1')
      )


    // -x-users----string ---- string --- string ----->
    timer(0, 1000)
      .pipe(
        switchMap(
          (count) => this.http.get<any>(`https://jsonplaceholder.typicode.com/posts/${count+1}`)
            .pipe(catchError(err => of(null)))
        ),
        switchMap(
          (post: any) => this.http.get<any>(`https://jsonplaceholder.typicode.com/users/${post.userId}`)
            .pipe(catchError(err => of(null)))
        ),
      )
      /*.subscribe(
        res => console.log('---->', res),
        err => console.log('err', err)
      )*/

  }

}
// =======

@Injectable({
  providedIn: 'root'
})
export class UserValidator {
  constructor(private  http: HttpClient) {
  }


  checkUsername(): AsyncValidatorFn {
    return (control: AbstractControl) => {
      return timer(1000)
        .pipe(
          switchMap(() => this.http.get<any[]>(`https://jsonplaceholder.typicode.com/users?username=${control.value}`)
            .pipe(
              map(res => {
                return res.length ? { notAvailable: true } : null
              }),
              catchError(err => of({ errorAPI: true}))
            ))
        )
    }
  }
}












// =======
function passwordMatch(): ValidatorFn {
  return (g: AbstractControl) => {
    const p1 = g.get('password1');
    const p2 = g.get('password2');
    if (p1?.value !== p2?.value) {
      p2?.setErrors({ ...p2?.errors, passwordDoesNotMatch: true})
      return { passNotMatch: true };
    } else {
      p2?.setErrors(null)
      return null
    }
  };
}

const ALPHA_NUMERIC_REGEX = /^\w+$/
export function alphaNumericValidator(c: FormControl): ValidationErrors | null {
  if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
    return {
      alphaNumeric: true
    }
  }
  return null;
}

