import { Component, Injector, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';

@Component({
  selector: 'cgm-my-input',
  template: `
    <div>{{label}}</div>
    <input type="text" [formControl]="input">
    <small *ngIf="ngControl.errors?.['required']">Required field</small>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: MyInputComponent,
      multi: true
    }
  ]
})
export class MyInputComponent implements ControlValueAccessor {
  @Input() label = ''
  onTouch!: () => void;
  input = new FormControl();
  ngControl!: NgControl;
  disabled: boolean = false;

  constructor(private inj: Injector) {}

  ngOnInit() {
    this.ngControl = this.inj.get(NgControl)
  }

  writeValue(text: string): void {
    this.input.setValue(text)
  }

  registerOnChange(fn: any): void {
    this.input.valueChanges
      .subscribe(fn)
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled
    if (isDisabled)
      this.input.disable()
    else
      this.input.enable()
  }

}
