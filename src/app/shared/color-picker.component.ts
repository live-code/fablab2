import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'cgm-color-picker',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div style="display: flex">
      <div
        class="cell"
        *ngFor="let c of colors"
        [style.background-color]="c"
        [ngClass]="{active: c === value}"
        (click)="selectColorHandler(c)"
      ></div>
      {{value}}
    </div>
  `,
  styles: [`
    .cell {width: 30px;height: 30px;}
    .active { border: 2px solid black}
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ColorPickerComponent,
      multi: true
    }
  ]
})
export class ColorPickerComponent implements ControlValueAccessor{

  @Input() colors = [
    '#F44336', '#90CAF9', '#FFCDD2', '#69F0AE', '#AED581', '#FFE082'
  ];

  value: string | null = null;
  onChange!: (color: string) => void;
  onTouch!: () => void;

  // from child to parent
  selectColorHandler(c: string) {
    this.value = c;
    this.onChange(c)
    this.onTouch()
  }

  // get value from parent
  writeValue(str: any): void {
    this.value = str;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
}
