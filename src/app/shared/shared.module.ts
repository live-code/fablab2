import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ColorPickerComponent } from './color-picker.component';
import { MyInputComponent } from './my-input.component';



@NgModule({
  declarations: [
    ColorPickerComponent,
    MyInputComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    ColorPickerComponent,
    MyInputComponent
  ]
})
export class SharedModule { }
