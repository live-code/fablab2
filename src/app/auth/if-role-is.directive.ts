import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { distinctUntilChanged } from 'rxjs';
import { AuthService } from '../core/auth.service';

@Directive({
  selector: '[cgmIfRoleIs]'
})
export class IfRoleIsDirective {
  @Input() cgmIfRoleIs!: string;
  constructor(
    private template: TemplateRef<any>,
    private authService: AuthService,
    private view: ViewContainerRef
  ) {

  }

  ngOnInit() {
    this.authService.role$
      .subscribe(role => {
        if (role === this.cgmIfRoleIs ) {
          this.view.createEmbeddedView(this.template)
        } else {
          this.view.clear();
        }
      })
  }


}
