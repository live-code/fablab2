import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { distinctUntilChanged } from 'rxjs';
import { AuthService } from '../core/auth.service';

@Directive({
  selector: '[cgmIfLogged]'
})
export class IfLoggedDirective {

  constructor(
    private template: TemplateRef<any>,
    private authService: AuthService,
    private view: ViewContainerRef
  ) {
    this.authService.isLogged$
      .pipe(
        distinctUntilChanged()
      )
      .subscribe(isLogged => {
        if (isLogged) {
          this.view.createEmbeddedView(template)
        } else {
          this.view.clear();
        }
      })
  }

}
