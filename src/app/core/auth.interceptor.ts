import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { catchError, delay, first, mergeMap, Observable, of, throwError, withLatestFrom } from 'rxjs';
import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  interceptOLDstyle(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = this.authService.data$.getValue()?.token
    let clonedReq = request;
    if (token) {
      clonedReq = request.clone({
        setHeaders: { Authorization: 'Bearer ' + token}
      })
    }
    return next.handle(clonedReq)
      .pipe(
        delay(environment.production ? 0 : 600)
      )
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
   return this.authService.isLogged$
     .pipe(
       first(),
       withLatestFrom(this.authService.token$),
       mergeMap(([isLogged, token ]) =>  {
         let clonedReq = request;
         if (isLogged) {
           clonedReq = request.clone({
             setHeaders: { Authorization: 'Bearer ' + token}
           })
         }
         return next.handle(clonedReq)
           .pipe(
             catchError(e => {
               switch (e.status) {
                 case 401:
                   alert('ahia!')
                   break;
                 default:
                   alert('default ahia!')
                   break;
               }
               return throwError(e);
             })
           )
       })
     )

  }

}
