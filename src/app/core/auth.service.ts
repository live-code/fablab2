import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, share } from 'rxjs';
import { Auth } from './auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  data$ = new BehaviorSubject<Auth | null>(null)

  constructor(private http: HttpClient) {
    const data = localStorage.getItem('auth')
    if (data) {
      this.data$.next(JSON.parse(data))
    }
  }

  login() {
    this.http.get<Auth>('http://localhost:3000/login')
      .subscribe(res => {
        this.data$.next(res)
        localStorage.setItem('auth', JSON.stringify(res))
      })
  }

  logout() {
    this.data$.next(null)
    localStorage.removeItem('auth')
  }

  get isLogged$() {
    return this.data$
      .pipe(
        map(data => !!data)
      )
  }

  get displayName$() {
    return this.data$
      .pipe(
        map(data => data?.displayName)
      )
  }

  get token$() {
    return this.data$
      .pipe(
        map(data => data?.token)
      )
  }
  get role$() {
    return this.data$
      .pipe(
        map(data => data?.role)
      )
  }
}
