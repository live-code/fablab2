export interface Auth {
  displayName: string
  role: string
  token: string
}
